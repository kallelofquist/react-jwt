export default class DecodedToken {
	constructor(header, payload, signature){
		this.header = header;
		this.payload = payload;
		this.signature = signature;
	}
};