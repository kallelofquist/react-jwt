import React from 'react';
import './App.css';
import Start from './components/Start';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          My React App
        </p>
      </header>
      <Start/>
    </div>
  );
}

export default App;