import Token from '../models/Token';

const JWTGetter = async () => {

	let JWTobject = new Token();

	await fetch('http://localhost:8080/sign')
	.then(response => response.json())
	.then(data => JWTobject.jwt = data.jwt);

	return JWTobject;

}

export default JWTGetter;