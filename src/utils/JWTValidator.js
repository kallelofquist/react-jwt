import DecodedToken from '../models/DecodedToken';

const JWTValidator = async (token) => {

	console.log(token);

	let myHeader = {
		'Authorization': token
	};

	let decodedToken = new DecodedToken();

	await fetch('http://localhost:8080/verify', {
		headers: myHeader,
  		method: 'GET'
	})
	.then(response => response.json())
	.then(data => decodedToken.payload = data);

	return decodedToken;

}

export default JWTValidator;