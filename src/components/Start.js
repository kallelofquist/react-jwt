import React from 'react';
import './Start.css';
import JWTGetter from '../utils/JWTGetter';
import JWTValidator from '../utils/JWTValidator';

class Start extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			token: '',
			decodedToken: ''
		};
	}

	async componentDidMount() {
	   const jwtToken = await JWTGetter();
	   this.setState({
	   	token: jwtToken
	   });
	}

	async validateJwt() {
		const tokenDecoded = await JWTValidator(this.state.token.jwt);
		console.log(tokenDecoded.payload);
		this.setState({
			decodedToken: tokenDecoded
		});
	}

	render() {

		return (
			<div>
				<h1>What can I say except you're welcome?</h1>

				<div id="inputBox">
					<textarea id="jwtBox" rows="10" cols="50" placeholder="JWT goes here..." value={this.state.token.jwt}>
					</textarea>
				</div>

				<div id="outputBoxes">
					<label>HEADER</label>
					<textarea id="header" rows="10" cols="40" placeholder="header goes here...">
					</textarea>
					<label>PAYLOAD</label>
					<textarea id="payload" rows="10" cols="40" placeholder="payload goes here..." value={this.state.decodedToken.payload}>
					</textarea>
					<label>SIGNATURE</label>
					<textarea id="signature" rows="10" cols="40" placeholder="signature goes here...">
					</textarea>
				</div>

				<div id="jwtBtns">
					<button>Generate</button>
					<button onClick={ () => this.validateJwt() }>Validate</button>
				</div>
			</div>
		)
	}
}

export default Start;